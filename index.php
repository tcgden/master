<?php

use App\Config\Controllers;
use App\Config\Routes;
use App\Config\Settings;
use System\Startup\Startup;

require __DIR__ . '/vendor/autoload.php';

$startup = new Startup(new Settings(), new Controllers(), new Routes());
$startup->execute();