<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 20/11/2020
 * Description:
 */

namespace System\Sockets;


use JsonException;
use Ratchet\ConnectionInterface;

class Message
{
    /**
     * @var ConnectionInterface
     */
    private ConnectionInterface $connection;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    public function ok($message = 'success'): void
    {
        /** @noinspection PhpUndefinedFieldInspection */
        $this->connection->send($this->encode([
            'status' => 'ok',
            'message' => $message,
            'id' => $this->connection->resourceId
        ]));
    }

    public function error($message = 'mayday!'): void
    {
        /** @noinspection PhpUndefinedFieldInspection */
        $this->connection->send($this->encode([
            'status' => 'error',
            'message' => $message,
            'id' => $this->connection->resourceId
        ]));
    }

    /**
     * @param array $value
     * @return string
     */
    private function encode(array $value): string
    {
        try {
            return json_encode($value, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            /** @noinspection PhpUndefinedFieldInspection */
            return '{"status":"fatal","message":"something terribly wrong with the server!","id":'.$this->connection->resourceId.'}';
        }
    }
}