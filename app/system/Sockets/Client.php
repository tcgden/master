<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 25/11/2020
 * Description:
 */

namespace System\Sockets;


use Exception;
use JsonException;
use System\Exceptions\GenericException;
use System\Exceptions\SocketException;
use System\Exceptions\SocketMessageErrorException;

class Client
{

    /**
     * @var false|resource
     */
    private $resource;

    /**
     * @var string
     */
    private string $host;

    /**
     * Client constructor.
     * @param string $host
     * @param int $port
     * @param array $headers
     * @param int $timeout
     * @param false $persistent
     * @param string $path
     * @param null $context
     * @throws SocketException
     */
    public function __construct(string $host='', int $port=80, array $headers = [], int $timeout=10, bool $persistent = false, string $path = '/', $context = null)
    {
        $this->host = $host;

        // Generate a key (to convince server that the update is not random)
        // The key is for the server to prove it i websocket aware. (We know it is)
        try {
            $key = base64_encode(random_bytes(16));
        } catch (Exception $e) {
            throw new SocketException($host . ': ' . $e->getMessage());
        }

        $header = "GET " . $path . " HTTP/1.1\r\n"
            ."Host: $host\r\n"
            ."pragma: no-cache\r\n"
            ."Upgrade: WebSocket\r\n"
            ."Connection: Upgrade\r\n"
            ."Sec-WebSocket-Key: $key\r\n"
            ."Sec-WebSocket-Version: 13\r\n";

        // Add extra headers
        if (count($headers)) {
            $header = implode("\r\n", $headers);
        }

        // Add end of header marker
        $header.="\r\n";
        $flags = STREAM_CLIENT_CONNECT | ( $persistent ? STREAM_CLIENT_PERSISTENT : 0 );
        $ctx = $context ?? stream_context_create ();
        $this->resource = stream_socket_client($host . ':' . $port, $errno, $errstr, $timeout, $flags, $ctx);
        if (!$this->resource) {
            throw new SocketException("Unable to connect to websocket server $host: $errstr ($errno)");
        }

        // Set timeouts
        stream_set_timeout($this->resource,$timeout);

        if (!$persistent || ftell($this->resource) === 0) {

            //Request upgrade to websocket
            $rc = fwrite($this->resource,$header);
            if (!$rc) {
                throw new SocketException("Unable to send upgrade header to websocket server $host: $errstr ($errno)");
            }

            // Read response into an assotiative array of headers. Fails if upgrade failes.
            $response_header=fread($this->resource, 1024);

            // status code 101 indicates that the WebSocket handshake has completed.
            if (strpos($response_header, ' 101 ') === false
                || stripos($response_header, 'Sec-WebSocket-Accept: ') === false) {
                throw new SocketException("Server $host did not accept to upgrade connection to websocket."
                    .$response_header. E_USER_ERROR);
            }
            // The key we send is returned, concatenate with "258EAFA5-E914-47DA-95CA-
            // C5AB0DC85B11" and then base64-encoded. one can verify if one feels the need...
        }

    }

    /**
     * @param array $parameter
     * @param bool $final
     * @return int
     * @throws SocketException
     */
    public function write(array $parameter, bool $final = true): int
    {
        // Assemble header: FINal 0x80 | Opcode 0x02
        $header=chr(($final?0x80:0) | 0x02); // 0x02 binary

        try {
            $data = json_encode($parameter, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            throw new SocketException($this->host . ': ' . $e->getMessage());
        }

        // Mask 0x80 | payload length (0-125)
        if(strlen($data)<126) {
            $header .= chr(0x80 | strlen($data));
        }
        elseif (strlen($data)<0xFFFF) {
            $header .= chr(0x80 | 126) . pack("n", strlen($data));
        }
        else {
            $header .= chr(0x80 | 127) . pack("N", 0) . pack("N", strlen($data));
        }

        // Add mask
        try {
            $mask = pack("N", random_int(1, 0x7FFFFFFF));
            $header.=$mask;

            // Mask application data.
            $length = strlen($data);
            for($i = 0; $i < $length; $i++) {
                $data[$i] = chr(ord($data[$i]) ^ ord($mask[$i % 4]));
            }

            $result = fwrite($this->resource,$header.$data);
            if ($result === false) {
                throw new GenericException('Error writing in the socket.');
            }
            return $result;
        } catch (Exception $e) {
            throw new SocketException($this->host . ': ' . $e->getMessage());
        }
    }

    /**
     * @return array
     * @throws SocketException|SocketMessageErrorException
     */
    public function read(): array
    {
        $data = "";
        do{
            // Read header
            $header = fread($this->resource,2);
            if (!$header) {
                throw new SocketException($this->host . ': ' . 'Reading header from websocket failed.');
            }

            $opcode = ord($header[0]) & 0x0F;
            $final = ord($header[0]) & 0x80;
            $masked = ord($header[1]) & 0x80;
            $payload_len = ord($header[1]) & 0x7F;

            // Get payload length extensions
            if ($payload_len >= 0x7E) {
                $ext_len = 2;
                if($payload_len === 0x7F) {
                    $ext_len = 8;
                }
                $header = fread($this->resource, $ext_len);
                if (!$header) {
                    throw new SocketException($this->host . ': ' . 'Reading header extension from websocket failed.');
                }

                // Set extended payload length
                $payload_len= 0;
                for($i=0; $i<$ext_len; $i++) {
                    $payload_len += ord($header[$i]) << ($ext_len - $i - 1) * 8;
                }
            }

            // Get Mask key
            if ($masked) {
                $mask = fread($this->resource,4);
                if (!$mask) {
                    throw new SocketException($this->host . ': ' . 'Reading header mask from websocket failed.');
                }
            }

            // Get payload
            $frame_data='';
            do {
                $frame= fread($this->resource,$payload_len);
                if (!$frame) {
                    throw new SocketException($this->host . ': ' . 'Reading from websocket failed.');
                }
                $payload_len -= strlen($frame);
                $frame_data.=$frame;
            } while($payload_len>0);

            // Handle ping requests (sort of) send pong and continue to read
            if ($opcode === 9) {
                // Assemble header: FINal 0x80 | Opcode 0x0A + Mask on 0x80 with zero payload
                fwrite($this->resource,chr(0x8A) . chr(0x80) . pack("N", random_int(1,0x7FFFFFFF)));
                continue;

                // Close
            }
            if ($opcode === 8) {
                fclose($this->resource);

                // 0 = continuation frame, 1 = text frame, 2 = binary frame
            } elseif ($opcode < 3) {
                // Unmask data
                $data_len=strlen($frame_data);
                if ($masked) {
                    for ($i = 0; $i < $data_len; $i++) {
                        $data .= $frame_data[$i] ^ $mask[$i % 4];
                    }
                }
                else {
                    $data .= $frame_data;
                }

            } else {
                continue;
            }

        } while(!$final);
        try {
            $response = json_decode($data, true, 512, JSON_THROW_ON_ERROR);
            $status = $response['status'] ?? 'ok';
            if ($status !== 'ok') {
                throw new SocketMessageErrorException(json_encode($response['message'], JSON_THROW_ON_ERROR));
            }
            return $response;
        } catch (JsonException $e) {
            throw new SocketException($this->host . ': ' . $e->getMessage());
        }
    }
}