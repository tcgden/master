<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 20/11/2020
 * Description:
 */

namespace System\Sockets;


use JsonException;
use Ratchet\ConnectionInterface;
use Ratchet\RFC6455\Messaging\MessageInterface;
use Slim\App;
use SplObjectStorage;

abstract class ACommands
{
    /**
     * @var App
     */
    private $app;

    /**
     * @var Message
     */
    private $message;

    /**
     * @var SplObjectStorage
     */
    private $clients;

    /**
     * @var ConnectionInterface
     */
    private $conn;

    /**
     * ACommands constructor.
     * @param App $app
     */
    public function __construct(App $app)
    {
        $this->app = $app;
    }

    /**
     * ACommands constructor.
     * @param ConnectionInterface $conn
     * @param SplObjectStorage $clientStorage
     */
    public function setConnections(ConnectionInterface $conn, SplObjectStorage $clientStorage): void
    {
        $this->message = new Message($conn);
        $this->clients = $clientStorage;
        $this->conn = $conn;
    }

    /**
     * @param MessageInterface $message
     * @return mixed
     */
    public function resolve(MessageInterface $message): void
    {
        try {
            $payload = json_decode($message->getPayload(), true, 512, JSON_THROW_ON_ERROR);
            foreach(get_class_methods($this) as $index) {
                if(preg_match("/\b$index\b$/", $payload['command'], $matches, PREG_OFFSET_CAPTURE, 0)) {

                    $this->$index($payload);
                }
            }
        } catch (JsonException $e) {
            $this->message->error('the input must be in an array format with command, arguments as index');
        }
    }

    public function help(): void
    {
        $this->message->ok(array_values(array_diff(get_class_methods($this), [
            'resolve',
            'getApp',
            '__construct',
            'getMessage',
            'setConnections',
            'getClients',
            'getConn'
        ])));
    }

    public function identify($payload): void
    {
        $this->clients->offsetSet($this->conn, $payload['arguments']);
    }

    public function ping(): void
    {
        $this->message->ok('pong');
    }

    public function report($payload): void
    {
        /** @var ConnectionInterface $client */
        foreach ($this->clients as $client) {
            if ($this->clients->offsetGet($client) === $payload['arguments']) {
                $message = new Message($client);
                $message->ok($payload['body']);
            }
        }
    }

    public function clients(): void
    {
        /** @var ConnectionInterface $client */
        $clients = [];
        foreach ($this->clients as $client) {
            $id = $this->clients->offsetGet($client);
            if ($this->conn !== $client && $id !== null) {
                $clients[] = $id;
            }
        }
        $this->message->ok($clients);
    }

    /**
     * @return Message
     */
    public function getMessage(): Message
    {
        return $this->message;
    }

    /**
     * @return SplObjectStorage
     */
    public function getClients(): SplObjectStorage
    {
        return $this->clients;
    }

    /**
     * @return ConnectionInterface
     */
    public function getConn(): ConnectionInterface
    {
        return $this->conn;
    }

    /**
     * @return App
     */
    protected function getApp(): App
    {
        return $this->app;
    }
}