<?php

namespace System\Storage;

use Exception;
use JsonException;
use PDO;
use PDOException;
use PDOStatement;
use System\Exceptions\EmptyException;
use System\Exceptions\GenericException;

/**
 * Class Database
 * @package System\Storage
 */
class Database {

    /**
     * @var string
     */
    private string $query;

    /**
     * @var array
     */
    private array $binds = [];

    /**
     * @var PDO
     */
    private $readHandler;

    /**
     * @var PDO
     */
    private $writeHandler;

    /**
     * @var bool
     */
    private bool $isReadOperation = false;

    /**
     * @var PDOStatement
     */
    private PDOStatement $stmt;

    /**
     * @var string
     */
    private string $name;

    /**
     * Database constructor.
     * @param $name
     */
    public function __construct($name) {
        $this->name = $name;
    }

    /**
     * @return PDO
     * @throws PDOException
     */
    public function getHandler(): PDO
    {
        if ($this->isReadOperation) {
            if (!$this->readHandler instanceof PDO) {
                $dsn = 'mysql:host=database.' . getenv('DOMAIN') . ';port=3307;dbname='. $this->name .';charset=utf8';
                $options = array(
                    PDO::ATTR_PERSISTENT => false,
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                );
                $this->readHandler = new PDO($dsn, 'root', getenv('MYSQL_ROOT_PASSWORD'), $options);
            }
            return $this->readHandler;
        }
        if (!$this->writeHandler instanceof PDO) {
            $dsn = 'mysql:host=database.' . getenv('DOMAIN') . ';port=3306;dbname='. $this->name .';charset=utf8';
            $options = array(
                PDO::ATTR_PERSISTENT => false,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            );
            $this->writeHandler = new PDO($dsn, 'root', getenv('MYSQL_ROOT_PASSWORD'), $options);
        }
        return $this->writeHandler;
    }

    /**
     * @param $query
     */
    private function query($query): void
    {
        $this->query = $query;
    }

    /**
     * @param string $query
     */
    public function read(string $query): void
    {
        $this->isReadOperation = true;
        $this->query($query);
    }

    /**
     * @param string $query
     */
    public function write(string $query): void
    {
        $this->isReadOperation = false;
        $this->query($query);
    }

    /**
     * @param string $param
     * @param string|int|bool $value
     * @param null $type
     */
    private function bind(string $param, $value, $type = null): void
    {
        if ($type === null) {
            switch (true) {
                case is_numeric($value):
                    if (strpos($value, '.')>0) {
                        $type = PDO::PARAM_STR;
                    } else {
                        $type = PDO::PARAM_INT;
                    }
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case $value === null:
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->binds[] = [
            'param' => $param,
            'value' => $value,
            'type' => $type
        ];
    }

    /**
     * @param array $array
     */
    public function bindArray(array $array): void
    {
        foreach ($array as $index => $item) {
            $this->bind($index, $item['value'], $item['type']);
        }
    }

    /**
     * @return bool
     * @throws GenericException|PDOException
     */
    public function execute(): bool
    {
        try {
            $this->stmt = $this->getHandler()->prepare($this->query);
            foreach($this->binds as $item) {
                $this->stmt->bindValue($item['param'], $item['value'], $item['type']);
            }
            $response = $this->stmt->execute();
            if ($response) {
                $this->query = '';
                $this->binds = [];
            }
            return $response;
        } catch (Exception  $e) {

            if (strpos($e->getMessage(), 'MySQL server has gone away')>-1) {
                // connection is dead;
                $this->readHandler = null;
                $this->writeHandler = null;
                $this->execute();
            }

            $genericException = new GenericException();
            $genericException->setMessage($e->getMessage());
            throw $genericException;
        }
    }

    /**
     * @return array
     * @throws GenericException
     * @throws PDOException
     */
    public function resultSet(): array
    {
        $this->isReadOperation = true;
        $this->execute();
        $results = $this->stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($results as $index => $values) {
            $results[$index] = $this->convertTypes($this->stmt, $values);
        }
        return $results;
    }

    /**
     * @return array
     * @throws EmptyException
     * @throws GenericException
     * @throws PDOException
     */
    public function single(): array
    {
        $this->isReadOperation = true;
        $this->execute();
        $results = $this->stmt->fetch(PDO::FETCH_ASSOC);
        if (is_array($results)) {
            return $this->convertTypes($this->stmt, $results);
        }
        throw new EmptyException();
    }

    /**
     * @return string
     * @throws PDOException
     */
    public function lastInsertId(): string
    {
        $this->isReadOperation = false;
        return $this->getHandler()->lastInsertId();
    }

    /**
     * Converts columns from strings to types according to
     * PDOStatement::columnMeta
     *
     * @param PDOStatement $statement
     * @param array $assoc returned by PDOStatement::fetch with PDO::FETCH_ASSOC
     * @return array copy of $assoc with matching type fields
     */
    public function convertTypes(PDOStatement $statement, array $assoc): array
    {
        for ($i = 0; $columnMeta = $statement->getColumnMeta($i); $i++)
        {
            if (isset($columnMeta['native_type'])) {
                switch($columnMeta['native_type'])
                {
                    case 'DECIMAL':
                    case 'TINY':
                    case 'SHORT':
                    case 'LONG':
                    case 'LONGLONG':
                    case 'INT24':
                        $assoc[$columnMeta['name']] = (int) $assoc[$columnMeta['name']];
                        break;
                    case 'DATETIME':
                    case 'DATE':
                    case 'TIMESTAMP':
                        $assoc[$columnMeta['name']] = strtotime($assoc[$columnMeta['name']]);
                        break;
                    // default: keep as string
                }
            } else if (isset($columnMeta['pdo_type']) && $columnMeta['pdo_type'] === 2) {
                try {
                    $assoc[$columnMeta['name']] = json_decode($assoc[$columnMeta['name']], true, 512, JSON_THROW_ON_ERROR);
                } catch (JsonException $e) {
                }
            }
        }
        return $assoc;
    }

}