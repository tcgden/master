<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 14/04/2020
 * Description:
 */

namespace System\Storage;


use Predis\Client;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use System\Utils\General;

/**
 * Class Session
 * @package App\Storage
 */
class Session
{

    /**
     * @var Client
     */
    private $redis;

    /**
     * Session constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $redisService = $container->get('Redis');
        $this->redis = $redisService->getClient();
        $settings = $container->get('settings');
        $database = 0;
        if (isset($settings['session']['database'])) {
            $database = $container->get('settings')['session']['database'];
        }
        $this->redis->select($database);
    }

    /**
     * @param Request $request
     * @return string
     * @noinspection PhpUnusedParameterInspection
     */
    private function createIndex(Request $request): string
    {
        return md5(General::getUserIP());
    }

    /**
     * @param $token
     * @param Request $request
     * @param $key
     */
    public function set($token, Request $request, $key): void
    {
        $keySession = $this->redis->get('key:'. $key);
        $sessions = unserialize($keySession, ['allowed_classes' => false]);
        if (!is_array($sessions)) {
            $sessions = [];
        }
        $sessions[$this->createIndex($request)] = $token;
        $this->redis->set('key:'. $key, serialize($sessions));
    }

    /**
     * @param $token
     * @param Request $request
     * @param $key
     * @return bool
     */
    public function has($token, Request $request, $key): bool
    {
        $keySession = $this->redis->get('key:'. $key);
        $sessions = unserialize($keySession, ['allowed_classes' => false]);
        if (!is_array($sessions)) {
            return false;
        }
        $index = $this->createIndex($request);
        return isset($sessions[$index])&&$sessions[$index] === $token;
    }

    /**
     * @param Request $request
     * @param $key
     * @return bool
     */
    public function remove(Request $request, $key): bool
    {
        $keySession = $this->redis->get('key:'. $key);
        $sessions = unserialize($keySession, ['allowed_classes' => false]);
        if (!is_array($sessions)) {
            return false;
        }
        unset($sessions[$this->createIndex($request)]);
        $this->redis->set('key:'. $key, serialize($sessions));
        return true;
    }
}