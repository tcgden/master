<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 18/04/2020
 * Description:
 */

namespace System\Storage;


use System\Repositories\ARepository;
use InvalidArgumentException;
use Psr\Container\ContainerInterface;
use Slim\Http\UploadedFile;
use System\Utils\General;

/**
 * Class Media
 * @package System\Storage
 */
class Media extends Order
{
    /**
     * @var File
     */
    private $storage;

    /**
     * @var string
     */
    private $endpoint;

    /**
     * @var null|string
     */
    private $scope = null;

    /**
     * @var string[]
     */
    private $types = array(
        'ai'      => 'application/postscript',
        'aif'     => 'audio/x-aiff',
        'aifc'    => 'audio/x-aiff',
        'aiff'    => 'audio/x-aiff',
        'asc'     => 'text/plain',
        'atom'    => 'application/atom+xml',
        'au'      => 'audio/basic',
        'avi'     => 'video/x-msvideo',
        'bcpio'   => 'application/x-bcpio',
        'bin'     => 'application/octet-stream',
        'bmp'     => 'image/bmp',
        'cdf'     => 'application/x-netcdf',
        'cgm'     => 'image/cgm',
        'class'   => 'application/octet-stream',
        'cpio'    => 'application/x-cpio',
        'cpt'     => 'application/mac-compactpro',
        'csh'     => 'application/x-csh',
        'css'     => 'text/css',
        'csv'     => 'text/csv',
        'dcr'     => 'application/x-director',
        'dir'     => 'application/x-director',
        'djv'     => 'image/vnd.djvu',
        'djvu'    => 'image/vnd.djvu',
        'dll'     => 'application/octet-stream',
        'dmg'     => 'application/octet-stream',
        'dms'     => 'application/octet-stream',
        'doc'     => 'application/msword',
        'dtd'     => 'application/xml-dtd',
        'dvi'     => 'application/x-dvi',
        'dxr'     => 'application/x-director',
        'eps'     => 'application/postscript',
        'etx'     => 'text/x-setext',
        'exe'     => 'application/octet-stream',
        'ez'      => 'application/andrew-inset',
        'gif'     => 'image/gif',
        'gram'    => 'application/srgs',
        'grxml'   => 'application/srgs+xml',
        'gtar'    => 'application/x-gtar',
        'hdf'     => 'application/x-hdf',
        'hqx'     => 'application/mac-binhex40',
        'htm'     => 'text/html',
        'html'    => 'text/html',
        'ice'     => 'x-conference/x-cooltalk',
        'ico'     => 'image/x-icon',
        'ics'     => 'text/calendar',
        'ief'     => 'image/ief',
        'ifb'     => 'text/calendar',
        'iges'    => 'model/iges',
        'igs'     => 'model/iges',
        'jpg'     => 'image/jpeg',
        'js'      => 'application/x-javascript',
        'json'    => 'application/json',
        'kar'     => 'audio/midi',
        'latex'   => 'application/x-latex',
        'lha'     => 'application/octet-stream',
        'lzh'     => 'application/octet-stream',
        'm3u'     => 'audio/x-mpegurl',
        'man'     => 'application/x-troff-man',
        'mathml'  => 'application/mathml+xml',
        'me'      => 'application/x-troff-me',
        'mesh'    => 'model/mesh',
        'mid'     => 'audio/midi',
        'midi'    => 'audio/midi',
        'mif'     => 'application/vnd.mif',
        'mov'     => 'video/quicktime',
        'movie'   => 'video/x-sgi-movie',
        'mp2'     => 'audio/mpeg',
        'mp3'     => 'audio/mpeg',
        'mp4'     => 'video/mp4',
        'mpe'     => 'video/mpeg',
        'mpeg'    => 'video/mpeg',
        'mpg'     => 'video/mpeg',
        'mpga'    => 'audio/mpeg',
        'ms'      => 'application/x-troff-ms',
        'msh'     => 'model/mesh',
        'mxu'     => 'video/vnd.mpegurl',
        'nc'      => 'application/x-netcdf',
        'oda'     => 'application/oda',
        'ogg'     => 'application/ogg',
        'pbm'     => 'image/x-portable-bitmap',
        'pdb'     => 'chemical/x-pdb',
        'pdf'     => 'application/pdf',
        'pgm'     => 'image/x-portable-graymap',
        'pgn'     => 'application/x-chess-pgn',
        'png'     => 'image/png',
        'pnm'     => 'image/x-portable-anymap',
        'ppm'     => 'image/x-portable-pixmap',
        'ppt'     => 'application/vnd.ms-powerpoint',
        'ps'      => 'application/postscript',
        'qt'      => 'video/quicktime',
        'ra'      => 'audio/x-pn-realaudio',
        'ram'     => 'audio/x-pn-realaudio',
        'ras'     => 'image/x-cmu-raster',
        'rdf'     => 'application/rdf+xml',
        'rgb'     => 'image/x-rgb',
        'rm'      => 'application/vnd.rn-realmedia',
        'roff'    => 'application/x-troff',
        'rss'     => 'application/rss+xml',
        'rtf'     => 'text/rtf',
        'rtx'     => 'text/richtext',
        'sgm'     => 'text/sgml',
        'sgml'    => 'text/sgml',
        'sh'      => 'application/x-sh',
        'shar'    => 'application/x-shar',
        'silo'    => 'model/mesh',
        'sit'     => 'application/x-stuffit',
        'skd'     => 'application/x-koan',
        'skm'     => 'application/x-koan',
        'skp'     => 'application/x-koan',
        'skt'     => 'application/x-koan',
        'smi'     => 'application/smil',
        'smil'    => 'application/smil',
        'snd'     => 'audio/basic',
        'so'      => 'application/octet-stream',
        'spl'     => 'application/x-futuresplash',
        'src'     => 'application/x-wais-source',
        'sv4cpio' => 'application/x-sv4cpio',
        'sv4crc'  => 'application/x-sv4crc',
        'svg'     => 'image/svg+xml',
        'svgz'    => 'image/svg+xml',
        'swf'     => 'application/x-shockwave-flash',
        't'       => 'application/x-troff',
        'tar'     => 'application/x-tar',
        'tcl'     => 'application/x-tcl',
        'tex'     => 'application/x-tex',
        'texi'    => 'application/x-texinfo',
        'texinfo' => 'application/x-texinfo',
        'tif'     => 'image/tiff',
        'tiff'    => 'image/tiff',
        'tr'      => 'application/x-troff',
        'tsv'     => 'text/tab-separated-values',
        'txt'     => 'text/plain',
        'ustar'   => 'application/x-ustar',
        'vcd'     => 'application/x-cdlink',
        'vrml'    => 'model/vrml',
        'vxml'    => 'application/voicexml+xml',
        'wav'     => 'audio/x-wav',
        'wbmp'    => 'image/vnd.wap.wbmp',
        'wbxml'   => 'application/vnd.wap.wbxml',
        'wml'     => 'text/vnd.wap.wml',
        'wmlc'    => 'application/vnd.wap.wmlc',
        'wmls'    => 'text/vnd.wap.wmlscript',
        'wmlsc'   => 'application/vnd.wap.wmlscriptc',
        'wrl'     => 'model/vrml',
        'xbm'     => 'image/x-xbitmap',
        'xht'     => 'application/xhtml+xml',
        'xhtml'   => 'application/xhtml+xml',
        'xls'     => 'application/vnd.ms-excel',
        'xml'     => 'application/xml',
        'xpm'     => 'image/x-xpixmap',
        'xsl'     => 'application/xml',
        'xslt'    => 'application/xslt+xml',
        'xul'     => 'application/vnd.mozilla.xul+xml',
        'xwd'     => 'image/x-xwindowdump',
        'xyz'     => 'chemical/x-xyz',
        'zip'     => 'application/zip'
    );

    /**
     * Media constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->storage = $container->get('Storage');
        $this->endpoint = $container->get('settings')['web_access'];
    }

    /**
     * @param ARepository $repository
     * @return $this
     */
    public function setScope(ARepository $repository): self
    {
        $this->scope = $repository->getTableName();
        return $this;
    }

    /**
     * @param integer $itemId
     * @param UploadedFile[] $collection
     */
    public function set($itemId, array $collection): void
    {
        try {
            $path = $this->storage->getPath('/media/' . $this->scope . '/' . $itemId);
            foreach($collection as $index => $file) {
                if (!is_dir($path. '/'. $index)) {
                    /** @noinspection MkdirRaceConditionInspection */
                    mkdir($path. '/'. $index, 0777, true);
                }
                if (is_array($file)) {
                    foreach($file as $each) {
                        $this->storeMedia($each, $path. '/'. $index);
                    }
                } else {
                    $this->storeMedia($file, $path. '/'. $index);
                }
            }
        } catch (InvalidArgumentException $e) {
            dump($e->getMessage());
        }
    }

    /**
     * @param UploadedFile $file
     * @param string $path
     * @throws \InvalidArgumentException
     */
    private function storeMedia(UploadedFile $file, $path): void
    {
        $extension = array_search($file->getClientMediaType(), $this->types, true);
        $fileName = md5($file->getClientFilename()) . '.' . $extension;
        $file->moveTo($path . '/' . $fileName);
        $order = $this->getOrder($path);
        $order[] = $fileName;
        $this->setOrder($path , $order);

    }

    /**
     * @param integer $itemId
     * @return array
     */
    public function get(int $itemId): array
    {
        $path = $this->storage->getPath('/media/' . $this->scope . '/' . $itemId);
        $response = [];
        if (is_dir($path)) {
            $fileCollection = General::array_diff(scandir($path), array('.', '..'));
            foreach($fileCollection as $name) {
                $order = $this->getOrder($path . '/' . $name);
                foreach($order as $each) {
                    $filePath = $path . '/' . $name . '/'. $each;
                    if (is_file($filePath)) {
                        $mime = current(explode('/', mime_content_type($filePath)));
                        $file = [
                            $mime =>   '/media/' . $this->scope . '/' . $itemId . '/' . $name . '/'. $each
                        ];
                        $response[$name][] = $file;
                    }
                }
            }
        }
        $response['endpoint'] = $this->endpoint;
        return $response;
    }

    /**
     * @param integer $itemId
     * @param string $folderName
     * @param string|null $fileName
     */
    public function remove(int $itemId, string $folderName, $fileName = null): void
    {
        $path = $this->storage->getPath('/media/' . $this->scope . '/' . $itemId . '/' . $folderName);
        if ($fileName !== null && is_file($path . '/' . $fileName)) {
            unlink($path . '/' . $fileName);
            $order = $this->getOrder($path);
            $index = array_search($fileName, $order, true);
            unset($order[$index]);
            $this->setOrder($path, array_values($order));
        } else if (is_dir($path)) {
            $fileCollection = General::array_diff(scandir($path), array('.', '..'));
            foreach($fileCollection as $name) {
                unlink($path . '/' . $name);
            }
            $this->removeOrder($path);
        }
    }

    /**
     * @param $itemId
     * @param $folderName
     * @param $order
     */
    public function order($itemId, $folderName, $order): void
    {
        $path = $this->storage->getPath('/media/' . $this->scope . '/' . $itemId . '/' . $folderName);
        $this->setOrder($path, $order);
    }

    /**
     * @return File
     */
    public function getStorage(): File
    {
        return $this->storage;
    }

    /**
     * @return string|null
     */
    public function getScope(): ?string
    {
        return $this->scope;
    }
}