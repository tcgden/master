<?php

namespace System\Storage;

use Exception;
use Predis\Autoloader;
use Predis\Client;

class Redis {

    /**
     * @var Client
     */
	private $client;

    /**
     * Redis constructor.
     * @param $scheme
     * @param $port
     */
	public function __construct($scheme, $port) {

		Autoloader::register();

		try {

			$this->client = new Client([
				'scheme' => $scheme,
				'host' => 'database.' . getenv('DOMAIN'),
				'port' => $port
			]);

		} catch (Exception $e) {
			echo $e->getMessage();
		}

	}

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }


}