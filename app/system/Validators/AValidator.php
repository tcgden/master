<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 08/05/2020
 * Description:
 */

namespace System\Validators;


use Awurth\SlimValidation\Validator;
use Psr\Container\ContainerInterface;
use System\Exceptions\ValidatorException;

abstract class AValidator
{
    /**
     * @var Validator
     */
    private $validator;

    /**
     * AValidator constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->validator = $container->get('Validator');
    }

    /**
     * @return Validator
     */
    protected function getValidator(): Validator
    {
        return $this->validator;
    }

    /**
     * @param $validator
     * @throws ValidatorException
     */
    protected function check($validator): void
    {
        if (!$validator->isValid()) {
            $errors = $validator->getErrors();
            $validatorException = new ValidatorException();
            $validatorException->setMessage($errors);
            throw $validatorException;
        }
    }
}