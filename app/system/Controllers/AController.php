<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 10/04/2020
 * Description:
 */

namespace System\Controllers;


use Mustache_Engine;
use Psr\Container\ContainerInterface;
use Slim\Container;
use Slim\Exception\ContainerException;
use System\Exceptions\AException;
use System\Exceptions\DeleteException;
use System\Exceptions\DuplicateException;
use System\Exceptions\EmptyException;
use System\Exceptions\GenericException;
use System\Exceptions\MissingException;
use System\Exceptions\SocketException;
use System\Repositories\ARepository;
use System\Requests\ARequest;
use System\Requests\LogRequest;
use System\Sockets\Client;
use System\Storage\Database;
use System\Storage\File;
use System\Storage\Media;
use InvalidArgumentException;
use Monolog\Logger;
use Slim\Http\Request;
use Slim\Http\Response;
use System\Validators\AValidator;

/**
 * Class AController
 * @package System\Controllers
 */
abstract class AController implements IController
{
    /**
     * @var Response
     */
    private $response;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var Container
     */
    private $container;

    /**
     * @var Media
     */
    private $media;

    /**
     * @var File
     */
    private $storage;

    /**
     * @var Database
     */
    private $database;

    /**
     * AController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return Container
     */
    public function getContainer(): Container
    {
        return $this->container;
    }

    /**
     * @param Response $response
     * @return AController
     */
    public function setResponse(Response $response): AController
    {
        $this->response = $response;
        return $this;
    }

    /**
     * @param $result
     * @return Response
     */
    public function success($result = null): Response
    {
        try {
            if (is_array($result)) {
                return $this->response->withJson([
                    'data' => $result
                ], 200)->withHeader('node', gethostname());
            }
            return $this->response->withStatus(204);
        } catch (InvalidArgumentException $e) {
            return $this->response;
        }
    }

    /**
     * @param AException|null $exception
     * @return Response
     */
    public function failure(AException $exception = null): Response
    {
        try {
            if ($exception !== null) {
                $statusCode = 400;
                if ($exception instanceof EmptyException) {
                    $statusCode = 404;
                }
                return $this->response->withJson([
                    'code' => $exception->getErrorCode(),
                    'error' => $exception->getErrorMessage()
                ], $statusCode)->withHeader('node', gethostname());
            }
            return $this->response->withStatus(400);
        } catch (InvalidArgumentException $e) {
            return $this->response;
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function list(Request $request, Response $response): Response
    {
        try {
            $results = [
                'fields' => $this->getRepository()->getTableStructure()->getFields($request),
                'items' => $this->getRepository()->getFilteredCollection($request)
            ];
            return $this->setResponse($response)->success($results);
        } catch (GenericException $e) {
            return $this->setResponse($response)->failure($e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @throws GenericException
     */
    public function get(Request $request, Response $response, array $args): Response
    {
        try {
            return $this->setResponse($response)->success($this->getRepository()->getFilteredItemById($request, $args['id']));
        } catch (EmptyException $e) {
            return $this->setResponse($response)->failure($e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function post(Request $request, Response $response, array $args): Response
    {
        try {
            $itemId = $this->getRepository()->storeItem($request->getParsedBody());
            return $this->setResponse($response)->success(['id' => $itemId]);
        } catch (DuplicateException | MissingException | GenericException $e) {
            return $this->setResponse($response)->failure($e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function put(Request $request, Response $response, array $args): Response
    {
        try {
            $repository = $this->getRepository();
            $repository->modifyItem($args['id'], $request->getParsedBody());
            return $this->setResponse($response)->success();
        } catch (DuplicateException | GenericException $e) {
            return $this->setResponse($response)->failure($e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function delete(Request $request, Response $response, array $args): Response
    {
        try {
            $this->getRepository()->deleteItem($request, $args['id']);
            return $this->setResponse($response)->success();
        } catch (DeleteException | EmptyException $e) {
            return $this->setResponse($response)->failure($e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function toggle(Request $request, Response $response, array $args): Response
    {
        try {
            $item = $this->getRepository()->getFilteredItemById($request, $args['id']);
            if (isset($item['isActive'])) {
                $isActive = (bool) $item['isActive'];
                $this->getRepository()->modifyItem($args['id'], [
                    'isActive' => (int) !$isActive
                ]);
            }
            return $this->setResponse($response)->success();
        } catch (EmptyException | DuplicateException | GenericException $e) {
            return $this->setResponse($response)->failure($e);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @throws GenericException
     */
    public function media(Request $request, Response $response, array $args): Response
    {
        $this->getMedia()->set($args['id'], $request->getUploadedFiles());
        $media = $this->getMedia()->get($args['id']);
        return $this->setResponse($response)->success($media);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @throws GenericException
     * @noinspection PhpUnusedParameterInspection
     */
    public function itemMedia(Request $request, Response $response, array $args): Response
    {
        return $this->setResponse($response)->success($this->getMedia()->get($args['id']));
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @throws GenericException
     */
    public function orderMedia(Request $request, Response $response, array $args): Response
    {
        $params = $request->getParsedBody();
        foreach($params as $folderName => $order) {
            $this->getMedia()->order($args['id'], $folderName, $order);
        }
        return $this->setResponse($response)->success();
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @throws GenericException
     */
    public function deleteMedia(Request $request, Response $response, array $args): Response
    {
        $params = $request->getParsedBody();
        foreach($params as $folderName => $fileName) {
            $this->getMedia()->remove($args['id'], $folderName, $fileName);
        }
        return $this->setResponse($response)->success();
    }

    /**
     * @param Request $request
     * @param string $identifier
     * @param int $itemId
     * @return string
     * @throws DuplicateException
     * @throws EmptyException
     * @throws GenericException
     * @noinspection PhpUnusedParameterInspection
     */
    public function activate(Request $request, string $identifier , int $itemId): string
    {
        $tableName = $this->getRepository()->getTableName();
        $uniqueIdentifierName = $this->getRepository()->getUniqueIdentifierName();
        /** @noinspection SqlNoDataSourceInspection */
        /** @noinspection SqlResolve */
        $this->getRepository()->getDatabase()->read("SELECT * FROM $tableName WHERE id = $itemId");
        $item = $this->getRepository()->getDatabase()->single();
        if (isset($item['isActive'])) {
            $isActive = (bool) $item['isActive'];
            $this->getRepository()->modifyItem($itemId, [
                'isActive' => (int) !$isActive
            ]);
            if ($item[$uniqueIdentifierName] === null) {
                //generate sku
                $amount = str_pad($this->getRepository()->countActives(),4,'0',STR_PAD_LEFT);
                $mustache = new Mustache_Engine();
                $rendered = $mustache->render($identifier . '{{date}}{{counter}}', [
                    'date' => date('Ymd'),
                    'counter' => $amount
                ]);
                $this->getRepository()->modifyItem($itemId, [
                    $uniqueIdentifierName => $rendered
                ]);
                return $rendered;
            }
            return $item[$uniqueIdentifierName];
        }
        throw new GenericException('this table dont have a uuid column or isActive column');
    }

    /**
     * You should return the right default repository to be used in this controller
     *
     * @return ARepository
     */
    abstract public function getRepository(): ARepository;

    /**
     * You should return the right default validator to be used in this controller
     *
     * @return AValidator
     */
    abstract public function getValidator(): AValidator;

    /**
     * @return Media
     * @throws GenericException
     */
    public function getMedia(): Media
    {
        if (!$this->media instanceof Media) {
            try {
                $this->media = $this->container->get('Media');
            } catch (ContainerException | InvalidArgumentException $e) {
                throw new GenericException($e->getMessage());
            }
            $this->media->setScope($this->getRepository());
        }
        return $this->media;
    }

    /**
     * @return Logger
     * @throws GenericException
     */
    public function getLogger(): Logger
    {
        if (!$this->logger instanceof Logger) {
            try {
                $this->logger = $this->container->get('Logger');
            } catch (ContainerException | InvalidArgumentException $e) {
                throw new GenericException($e->getMessage());
            }
        }
        return $this->logger;
    }

    /**
     * @return File
     * @throws GenericException
     */
    public function getStorage(): File
    {
        if (!$this->storage instanceof File) {
            try {
                $this->storage = $this->container->get('Storage');
            } catch (ContainerException | InvalidArgumentException $e) {
                throw new GenericException($e->getMessage());
            }
        }
        return $this->storage;
    }

    /**
     * @return Database
     * @throws GenericException
     */
    public function getDatabase(): Database
    {
        if (!$this->database instanceof Database) {
            try {
                $this->database = $this->container->get('Database');
            } catch (ContainerException | InvalidArgumentException $e) {
                throw new GenericException($e->getMessage());
            }
        }
        return $this->database;
    }

    /**
     * @return LogRequest
     */
    public function getLogReporter(): ARequest
    {
        return LogRequest::getInstance($this->getContainer());
    }

    /**
     * @param string $name
     * @param string $email
     * @param string $subject
     * @param string $template
     * @param array $content
     * @param array $attachments
     * @throws SocketException
     */
    public function sendEmail(string $name, string $email, string $subject, string $template, array $content = [], array $attachments = []): void
    {
        $emails = new Client('tasks.api_emails', 8080);
        $emails->write([
            'command' => 'send',
            'arguments' => [
                'name' => $name,
                'email' => $email,
                'subject' => $subject,
                'template' => file_get_contents($template . '.tmpl'),
                'content' => $content,
                'attachments' => $attachments
            ]
        ]);
    }
}