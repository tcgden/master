<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 14/05/2021
 * Description:
 */

namespace System\Repositories\Structure;


abstract class AType
{

    /**
     * @var array
     */
    protected array $fieldNames = [];

    /**
     * @var array
     */
    protected array $bindValues = [];

    /**
     * AType constructor.
     * @param array $data
     * @param array $structure
     */
    abstract public function __construct(array $data, array $structure);

    /**
     * @return array
     */
    public function getFieldNames() {
        return $this->fieldNames;
    }

    /**
     * @return array
     */
    public function getBindValues() {
        return $this->bindValues;
    }
}