<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 14/05/2021
 * Description:
 */

namespace System\Repositories\Structure;


use PDO;

class Select extends AType
{
    /**
     * Select constructor.
     * @param array $data
     * @param array $structure
     */
    public function __construct(array $data, array $structure)
    {
        $filtered = array_intersect_key($structure, $data);
        foreach($filtered as $field => $type) {
            $pdoValue = $data[$field];
            $pdoField = "$field = :$field";
            $pdoType = PDO::PARAM_STR;
            if ($type === 'json') {
                $pdoField = "$field REGEXP :$field";
            } else if (in_array($type, ['int', 'date', 'datetime'])) {
                $pdoType = PDO::PARAM_INT;
            }
            if (is_array($pdoValue)) {
                $pdoField = "$field in( '". implode("','", $pdoValue) . "' )";
            }
            $this->fieldNames[] = $pdoField;
            $this->bindValues[':' . $field] = [
                'type' => $pdoType,
                'value' => $pdoValue
            ];
        }
    }
}