<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 14/05/2021
 * Description:
 */

namespace System\Repositories\Structure;


use JsonException;
use PDO;

class Update extends AType
{

    /**
     * Select constructor.
     * @param array $data
     * @param array $structure
     */
    public function __construct(array $data, array $structure)
    {
        $filtered = array_intersect_key($structure, $data);
        foreach($filtered as $field => $type) {
            if ($field === 'deleted' && $data[$field] === null) {
                $this->fieldNames[] = "$field = :$field";
                $this->bindValues[':' . $field] = [
                    'type' => PDO::PARAM_NULL,
                    'value' => null
                ];
            } else {
                try {
                    $pdoValue = $data[$field];
                    $pdoField = "$field = :$field";
                    $pdoType = PDO::PARAM_STR;
                    if ($type === 'json') {
                        $pdoValue = json_encode($pdoValue, JSON_THROW_ON_ERROR);
                    } else if (strpos($type, 'int')>-1) {
                        $pdoType = PDO::PARAM_INT;
                    }
                    $this->fieldNames[] = $pdoField;
                    $this->bindValues[':' . $field] = [
                        'type' => $pdoType,
                        'value' => $pdoValue
                    ];
                } catch (JsonException $e) {}
            }
        }
    }

}