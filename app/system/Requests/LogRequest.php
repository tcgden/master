<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 21/07/2020
 * Description:
 */

namespace System\Requests;

use Mustache_Engine;
use Mustache_Exception_InvalidArgumentException;
use System\Exceptions\AException;
use System\Exceptions\SocketException;
use System\Sockets\Client;

/**
 * Class LogRequest
 * @package App\Requests
 */
class LogRequest extends ARequest
{
    /**
     * @var string
     */
    private string $action = '';

    /**
     * @var string
     */
    private string $scope = '';

    /**
     * @var string
     */
    private string $reference = '';

    /**
     * @var string
     */
    private string $parent = '';

    /**
     * @var array
     */
    private array $content = [];

    /**
     * @param string|null $description
     */
    public function report(string $description = null): void
    {
        try {
            $logs = new Client('tasks.api_logs', 8080);
            $mustache = new Mustache_Engine(array('entity_flags' => ENT_QUOTES));
            $params = [
                'scope' => $this->scope,
                'reference' => $this->reference,
                'action' => $this->action,
                'description' => $description,
                'parent' => $this->parent,
                'content' => $this->content
            ];
            $params['description'] = $mustache->render($description, $params);
            $logs->write([
                'command' => 'create',
                'arguments' => $params,
                'report' => 'office'
            ]);
        } catch (SocketException | Mustache_Exception_InvalidArgumentException $e) {
            $this->getLogger()->error($e->getMessage());
        }
    }

    /**
     * @param AException $e
     */
    public function exception(AException $e): void
    {
        try {
            $logs = new Client('tasks.api_logs', 8080);
            $params = [
                'scope' => $this->scope,
                'reference' => $this->reference,
                'action' => 'exception',
                'description' => $e->getMessage(),
                'parent' => $this->parent,
                'content' => getallheaders()
            ];
            $logs->write([
                'command' => 'create',
                'arguments' => $params,
                'report' => 'office'
            ]);
        } catch (SocketException $e) {
            $this->getLogger()->error($e->getMessage());
        }
    }

    /**
     * @param string $scope
     */
    public function setScope(string $scope): void
    {
        $this->scope = $scope;
    }

    /**
     * @param string $reference
     */
    public function setReference(string $reference): void
    {
        $this->reference = $reference;
    }

    /**
     * @param string $parent
     */
    public function setParent(string $parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @return array
     */
    public function getContent(): array
    {
        return $this->content;
    }

    /**
     * @param array $content
     */
    public function setContent(array $content): void
    {
        $this->content = array_merge($this->content, $content);
    }

    /**
     * @param string $action
     */
    public function setAction(string $action): void
    {
        $this->action = $action;
    }
}