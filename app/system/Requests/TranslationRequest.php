<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 21/07/2020
 * Description:
 */

namespace System\Requests;

use System\Exceptions\SocketException;
use System\Exceptions\SocketMessageErrorException;
use System\Repositories\ARepository;
use System\Sockets\Client;

/**
 * Class ContentRequest
 * @package App\Requests
 */
class TranslationRequest
{
    /**
     * @param ARepository $repository
     * @param array $item
     * @param string $lang
     * @throws SocketException
     * @throws SocketMessageErrorException
     */
    public function content(ARepository $repository, array &$item, string $lang): void
    {
        $client = new Client('tasks.api_translates', 8080);
        $uuid = $item[$repository->getUniqueIdentifierName()];
        $client->write(['command' => 'item', 'arguments' => ['reference' => $uuid, 'lang' => $lang]]);
        $translation = $client->read();
        if (isset($translation['message']['content'])) {
            foreach($translation['message']['content'] as $index => $value) {
                $item[$index] = $value;
            }
        }
    }
}