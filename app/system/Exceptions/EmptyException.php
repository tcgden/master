<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 14/04/2020
 * Description:
 */

namespace System\Exceptions;


use Throwable;

class EmptyException extends AException
{
    public function __construct($message = 'No resource was found!', $code = 2, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function getErrorMessage(): string
    {
        return $this->getMessage();
    }

    public function getErrorCode(): int
    {
        return 2;
    }

    public function setMessage($message): void
    {
        // TODO: Implement setMessage() method.
    }
}