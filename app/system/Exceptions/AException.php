<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 14/04/2020
 * Description:
 */

namespace System\Exceptions;


use Exception;

abstract class AException extends Exception
{
    protected $message;

    public function getErrorMessage()
    {
        return $this->message;
    }

    abstract public function getErrorCode(): int;

    abstract public function setMessage($message): void;
}