<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 15/04/2020
 * Description:
 */

namespace System\Exceptions;




class MissingException extends AException
{
    public function getErrorMessage(): string
    {
        preg_match('/Field \'.*\' doesn\'t have a default value/m', $this->getMessage(), $matches);
        return end($matches);
    }

    public function getErrorCode(): int
    {
        return 4;
    }

    public function setMessage($message): void
    {
        $this->message = $message;
    }
}