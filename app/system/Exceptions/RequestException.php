<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 28/08/2020
 * Description:
 */

namespace System\Exceptions;


/**
 * Class RequestException
 * @package System\Exceptions
 */
class RequestException extends AException
{

    /**
     * @return int
     */
    public function getErrorCode(): int
    {
        return 666;
    }

    /**
     * @param string $message
     */
    public function setMessage($message): void
    {
        $this->message = $message;
    }
}