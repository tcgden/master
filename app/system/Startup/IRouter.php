<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 09/11/2020
 * Description:
 */

namespace System\Startup;


use Slim\App;

interface IRouter
{
    public function export(App $app): void;
}