<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 09/11/2020
 * Description:
 */

namespace System\Startup;


interface ISettings
{
    public function export(): array;
}