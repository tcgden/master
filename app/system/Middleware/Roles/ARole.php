<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 12/04/2020
 * Description:
 */

namespace System\Middleware\Roles;


use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

abstract class ARole implements IRole
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * ARole constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    private function getRoleName() {
        $classNameExploded = explode('\\', static::class);
        return end($classNameExploded);
    }

    /**
     * @inheritDoc
     */
    public function __invoke(Request $request, Response $response, $next)
    {
        $user = $request->getAttribute('user');
        $roles = (array) $user->role;
        $userRoleName = null;
        /** @noinspection LoopWhichDoesNotLoopInspection */
        foreach($roles as $userRoleName => $unused) {
            break;
        }
        $roleName = $this->getRoleName();
        if (!$this->checkPermission($userRoleName, $roleName)) {
            return $response->withStatus(403, "You need at least $roleName permission for this!");
        }
        return $next($request, $response);
    }

    abstract public function checkPermission($userRoleName, $roleName): bool;

    abstract public static function getName(): string;

    public static $id;

    /**
     * @param array $role
     * @return array
     */
    public static function checkTree(array $role): array
    {
        switch ($role['name']) {
            /** @noinspection PhpMissingBreakStatementInspection */ case God::getName():
                $roles[God::getName()] = 'allow';
            /** @noinspection PhpMissingBreakStatementInspection */ case Admin::getName():
                $roles[Admin::getName()] = 'allow';
            case Editor::getName():
                $roles[Editor::getName()] = 'allow';
        }
        $roles[User::getName()] = 'allow';
        return $roles;
    }
}