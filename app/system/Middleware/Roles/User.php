<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 12/04/2020
 * Description:
 */

namespace System\Middleware\Roles;


class User extends ARole
{
    static public $id = 4;

    public function checkPermission($userRoleName, $roleName): bool
    {
        return $userRoleName === $roleName;
    }

    public static function getName(): string
    {
        return 'User';
    }
}