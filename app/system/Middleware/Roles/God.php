<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 12/04/2020
 * Description:
 */

namespace System\Middleware\Roles;



class God extends ARole
{
    static public $id = 1;

    public function checkPermission($userRoleName, $roleName): bool
    {
        switch ($userRoleName) {
            case self::getName():
            case Admin::getName():
            case Editor::getName():
                return true;
            default:
                return false;
        }
    }


    public static function getName(): string
    {
        return 'God';
    }
}