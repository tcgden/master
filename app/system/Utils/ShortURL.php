<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 15/04/2020
 * Description:
 */

namespace System\Utils;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use PHPHtmlParser\Dom;
use PHPHtmlParser\Exceptions\ChildNotFoundException;
use PHPHtmlParser\Exceptions\CircularException;
use PHPHtmlParser\Exceptions\CurlException;
use PHPHtmlParser\Exceptions\NotLoadedException;
use PHPHtmlParser\Exceptions\StrictException;

class ShortURL
{

    /**
     * @var ShortURL
     */
    static private  $instance;

    /**
     * @var Client
     */
    private $client;

    /**
     * @return ShortURL
     */
    public static function getInstance(): ShortURL
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param $url
     * @return string
     */
    public function create($url): string
    {
        try {
            $response = $this->client->request('POST', 'https://tinyurl.com/create.php', ['form_params' => ['url' => $url]]);
            $dom = new Dom;
            $dom->load($response->getBody()->getContents());
            $articleCollection = $dom->find('.mainpane #contentcontainer .indent b');
            return $articleCollection->toArray()[1]->innerHtml();
        } catch (GuzzleException | ChildNotFoundException | NotLoadedException | CircularException | CurlException | StrictException $e) {
            return $url;
        }
    }
}