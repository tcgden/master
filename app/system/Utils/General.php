<?php

namespace System\Utils;

class General {

	public static function isEmailValid($str): bool
    {
		return (bool)filter_var($str, FILTER_VALIDATE_EMAIL);
	}

	public static function sanitizeEmail($str) {
		return filter_var($str, FILTER_SANITIZE_EMAIL);
	}

	public static function hashPassword($str) {
		return password_hash($str, PASSWORD_BCRYPT);
	}

	public static function currentDate() {
		return date('Y-m-d H:i:s', time());
	}

    /**
     * @param string $string
     * @param string $separator
     * @return string
     */
    public static function format_uri(string $string, string $separator = '-' ): string
    {
        $string = utf8_encode($string);
        $string = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
        $string = preg_replace('/[^a-z0-9- ]/i', '', $string);
        /** @noinspection CallableParameterUseCaseInTypeContextInspection */
        $string = str_replace(' ', $separator, $string);
        $string = trim($string, $separator);
        $string = strtolower($string);
        if (empty($string)) {
            return 'n-a';
        }
        return $string;
    }

    /**
     * @param $dir
     */
    public static function rrmdir($dir): void
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object !== '.' && $object !== '..') {
                    if (filetype($dir . '/' . $object) === 'dir') {
                        self::rrmdir($dir . '/' . $object);
                    }
                    else {
                        unlink($dir . '/' . $object);
                    }
                }
            }
            reset($objects);
            self::rrmdir($dir);
        }
    }

    /**
     * @param $a
     * @param $b
     * @return array
     */
    public static function array_diff($a, $b): array
    {
        $map = array();
        foreach($a as $val) {
            $map[$val] = 1;
        }
        foreach($b as $val) {
            unset($map[$val]);
        }
        return array_keys($map);
    }

    /**
     * @return string
     */
    public static function getUserIP(): string
    {
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP)) { $ip = $client; }
        elseif(filter_var($forward, FILTER_VALIDATE_IP)) { $ip = $forward; }
        else { $ip = $remote; }

        return $ip;
    }
}