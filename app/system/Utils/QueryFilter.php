<?php
/**
 * TravelCentral24
 * User: Fábio Menezes
 * Date: 03/04/2021
 * Description:
 */

namespace System\Utils;

/**
 * Class QueryFilter
 * @package System\Utils
 */
class QueryFilter
{
    private string $type;
    private string $search;

    public function __construct(string $type, string $search)
    {
        $this->type = $type;
        $this->search = $search;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getSearch(): string
    {
        return $this->search;
    }

}